package com.open1111.util;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * ajax返回输出工具类
 */
public class ResponseUtil {

    public static void write(HttpServletResponse response, Object o) throws Exception {
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.print(o.toString());
        out.flush();
        out.close();
    }

}
