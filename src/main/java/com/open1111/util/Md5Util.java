package com.open1111.util;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * md5加密工具类
 */
public class Md5Util {

    public static final String SALT = "open1111";

    /**
     * md5加密
     * @param str
     * @param salt
     * @return
     */
    public static String md5(String str, String salt) {
        return new Md5Hash(str, salt).toString();
    }

    public static void main(String[] args) {
        String password = "123456";
        System.out.println("加密后: " + md5(password, SALT));
    }

}
