package com.open1111.service;

import com.open1111.entity.Manager;

/**
 * 管理员service接口
 */
public interface ManagerService {

    public Manager getByUserName(String userName);

}
