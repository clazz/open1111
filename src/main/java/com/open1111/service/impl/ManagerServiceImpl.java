package com.open1111.service.impl;

import com.open1111.dao.ManagerDao;
import com.open1111.entity.Manager;
import com.open1111.service.ManagerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 管理员service实现类
 */
@Service("managerService")
public class ManagerServiceImpl implements ManagerService {

    @Resource
    private ManagerDao managerDao;

    public Manager getByUserName(String userName) {
        return managerDao.getByUserName(userName);
    }

}
