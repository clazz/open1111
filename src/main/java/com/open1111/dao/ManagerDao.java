package com.open1111.dao;

import com.open1111.entity.Manager;

/**
 * 管理员Dao接口
 */
public interface ManagerDao {

    /**
     * 通过用户名查询用户
     * @param userName
     * @return
     */
    Manager getByUserName(String userName);




}
